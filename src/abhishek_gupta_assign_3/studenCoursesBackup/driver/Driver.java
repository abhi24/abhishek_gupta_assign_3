package abhishek_gupta_assign_3.studenCoursesBackup.driver;

import abhishek_gupta_assign_3.studenCoursesBackup.myTree.BSTBuilder;
import abhishek_gupta_assign_3.studenCoursesBackup.util.Results;

public class Driver {

	public static void main(String[] args) {

		String inputFile = null;
		String deleteFile = null;
		String output = null;
		String output1 = null;
		String output2 = null;

		try {
			inputFile = args[0];
			if (!(inputFile.equals("input.txt"))) {
				System.out.println("No Input File Present Please Enter It");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("No Input File Present Please Enter It");
			System.exit(0);
		}
		try {
			deleteFile = args[1];
			if (!(deleteFile.equals("delete.txt"))) {
				System.out.println("No file present to delete values from Nodes");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("No file present to delete values from Nodes");
			System.exit(0);
		}
		try {
			output = args[2];
			if (!(output.equals("output1.txt"))) {
				System.out.println("No Output File Present Please Enter It");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("No Output File Present Please Enter It");
			System.exit(0);
		}
		try {
			output1 = args[3];
			if (!(output1.equals("output2.txt"))) {
				System.out.println("No Output File Present Please Enter It");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("No Output File Present Please Enter It");
			System.exit(0);
		}
		try {
			output2 = args[4];
			if (!(output2.equals("output3.txt"))) {
				System.out.println("No Output File Present Please Enter It");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("No Output File Present Please Enter It");
			System.exit(0);
		}

		BSTBuilder builder = new BSTBuilder();
		builder.create(inputFile);
		Results res = new Results();

		res.printViaStd("Printing elements of the Main Node");
		res.printViaFile("Printing elements of the Main Node");

		res.printViaStd("___________________________________");
		res.printViaFile("__________________________________");

		builder.printInOrder();

		builder.deleteCreateValue(deleteFile);

	}

}
